// @flow
import 'App/Config/ReactotronConfig';
import { AppRegistry } from 'react-native';
import Root from './src/Root';

AppRegistry.registerComponent('RNSkeletonApp', () => Root);
