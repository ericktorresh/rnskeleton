import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addNavigationHelpers, StackNavigator } from 'react-navigation';
import HomeContainer from '@containers/HomeContainer';
import HelloContainer from '@containers/HelloContainer';

export const AppNavigator = StackNavigator({
	Home: { screen: HomeContainer },
	Hello: { screen: HelloContainer },
});

const AppNavigatorWithNavigationState = ({ dispatch, nav }) => (
	<AppNavigator navigation={addNavigationHelpers({ dispatch, state: nav })} />
);

AppNavigatorWithNavigationState.propTypes = {
	dispatch: PropTypes.func.isRequired,
	nav: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
	nav: state.nav,
});

export default connect(mapStateToProps)(AppNavigatorWithNavigationState);
