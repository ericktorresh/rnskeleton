// @flow
import { Dimensions, Platform } from 'react-native';

const { width, height } = Dimensions.get('window');

const IOS_NAV_BAR_HEIGHT = 44;
const IOS_STATUS_BAR_HEIGHT = 20;
const ANDROID_NAV_BAR_HEIGHT = 30;
const ANDROID_STATUS_BAR_HEIGHT = 24;

// Used via Metrics.baseMargin
export default metrics = {
	marginHorizontal: 20,
	marginVertical: 20,
	paddingHorizontal: 20,
	paddingVertical: 20,
	baseMargin: 10,
	basePadding: 10,
	normalMargin: 15,
	doubleBaseMargin: 20,
	smallMargin: 5,
	smallPadding: 5,
	horizontalLineHeight: 1,
	screenWidth: width < height ? width : height,
	screenHeight: width < height ? height : width,
	statusBarHeight: Platform.OS === 'ios' ? IOS_STATUS_BAR_HEIGHT : ANDROID_STATUS_BAR_HEIGHT,
	navBarHeight: Platform.OS === 'ios' ? IOS_NAV_BAR_HEIGHT : ANDROID_NAV_BAR_HEIGHT,
	navTabHeight: 50,
	borderRadius: 4,
	settingsItemSize: 45,
	borderWidth: 1,
	icons: {
		tiny: 15,
		small: 20,
		medium: 30,
		large: 45,
		xl: 60,
	},
	images: {
		small: 20,
		medium: 40,
		large: 60,
		logo: 300,
	},
	font: {
		normal: 12,
		medium: 16,
		large: 18,
	}
};
