// @flow
import Metrics from './Metrics';
import Colors from './Colors';

const defaultButtonStyle = {
	paddingVertical: 10,
	paddingHorizontal: 20,
	borderRadius: Metrics.borderRadius,
	borderWidth: 1,
	borderColor: Colors.greyColor1,
	backgroundColor: Colors.greyColor2,
	alignItems: 'center',
};

const defaultButtonText = {
	fontSize: 12,
	textAlign: 'center',
	fontWeight: '700',
};

export default ApplicationStyles = {
	screen: {
		application: {
			flex: 1,
			backgroundColor: Colors.backgroundSolidColor,
		},

		container: {
			flex: 1,
			backgroundColor: Colors.backgroundSolidColor,
		},

		whiteBackground: {
			backgroundColor: Colors.whiteColor,
		},

		containerWithPadding: {
			paddingHorizontal: Metrics.marginHorizontal,
			paddingVertical: Metrics.marginVertical,
		},

		containerWithSmallPadding: {
			paddingHorizontal: Metrics.smallPadding,
			paddingVertical: Metrics.marginVertical,
		},

		containerWithHeader: {
			marginTop: Metrics.navBarHeight + Metrics.statusBarHeight,
		},

		containerWithoutHeader: {
			marginTop: Metrics.statusBarHeight,
		},

		containerWithTabs: {
			marginBottom: Metrics.navTabHeight,
		},

		centering: {
			flex: 1,
			alignItems: 'center',
			justifyContent: 'center',
			padding: 8,
		},

		section: {
			marginBottom: Metrics.baseMargin,
			paddingVertical: Metrics.baseMargin,
			paddingHorizontal: Metrics.baseMargin,
			borderWidth: 1,
			borderColor: Colors.greyColor6,
			backgroundColor: Colors.whiteColor,
			shadowColor: Colors.greyColor5,
			shadowOffset: {
				width: 0,
				height: 2
			},
			shadowRadius: 4,
			shadowOpacity: 0.6,
		},
	},

	button: {
		defaultButton: {
			...defaultButtonStyle
		},

		defaultButtonText: {
			...defaultButtonText,
			color: Colors.textColor,
		},

		disabledButton: {
			opacity: 0.5,
		},

		primaryButton: {
			...defaultButtonStyle,
			backgroundColor: Colors.buttonColor1,
			borderColor: Colors.greyColor6,
		},

		primaryButtonText: {
			...defaultButtonText,
			color: Colors.whiteColor,
		},

		backButtonWrapper: {
			position: 'absolute',
			top: 20,
			left: 10,
			width: 20,
			backgroundColor: 'transparent',
			zIndex: 99,
		},

		backButtonIcon: {
			color: Colors.headingInvertedColor,
			fontSize: Metrics.icons.medium,
			fontWeight: '200',
		},
	},

	text: {
		regularText: {
			color: Colors.textColor,
			fontSize: 14,
		},

		mainColor: {
			color: Colors.mainColor,
		},

		mutedText: {
			color: Colors.greyColor9,
		},

		invertedText: {
			color: Colors.whiteColor,
		},

		heading1: {
			color: Colors.headingColor,
			fontSize: 20,
		},

		heading2: {
			color: Colors.headingColor,
			fontSize: 18,
		},

		heading3: {
			color: Colors.headingColor,
			fontSize: 16,
		},

		textChoice: {
			color: Colors.headingColor,
			fontSize: 16
		},

		link: {
			color: Colors.activeColor,
		}
	}
};
