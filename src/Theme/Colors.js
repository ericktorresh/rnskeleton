// @flow

export default colors = {
	backgroundSolidColor: '#fff',

	mainColor: '#ccc',
	secundaryColor: '#333',
	mutedSelectedColor: '#82bdd9',

	transparent: 'transparent',
	activeColor: '#0090e3',
	whiteColor: '#ffffff',
	blackColor: '#000000',

	headingInvertedColor: '#ffffff',
	headingColor: '#000000',
	textColor: '#666666',
};
