import LocalizedStrings from 'react-native-localization';
import enStrings from './locate/en-Us';

const strings = new LocalizedStrings({
	en: enStrings
});

export default strings;
