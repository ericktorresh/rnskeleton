export default strings = {
	token  : 'Token: {0}',
	home   : 'Home',
	hello  : 'Hello',
	goTo   : 'Go to {0}',
	goBack : 'Go Back',
};
