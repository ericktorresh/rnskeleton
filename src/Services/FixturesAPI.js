
export default {
	// Functions return fixtures
	fetchToken: () => {
		return new Promise((resolve) => {
			const token = (new Date()).getTime().toString();
			
			setTimeout(() => {
				resolve({
					data: { token },
					ok: true,
					problem: ''
				});
			}, 0);
		});
	},
};
