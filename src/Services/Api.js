// a library to wrap and simplify api calls
import apisauce from 'apisauce';
import Config from '@config';

const create = (baseURL = Config.API.baseUrl) => {
	const api = apisauce.create({
		baseURL,
		headers: {
			'Cache-Control': 'no-cache',
			'Content-Type': 'application/json',
		},
		timeout: Config.API.timeout
	});

	if (__DEV__) {
		api.addMonitor(response => console.log('response: ', response));
	}

	const fetchToken = () => {
		return api.get('token');
	};

	return {
		fetchToken,
	};
};

// let's return back our create method as the default.
export default {
	create,
};
