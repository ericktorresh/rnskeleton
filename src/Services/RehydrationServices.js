// @flow
import { AsyncStorage } from 'react-native';
import { persistStore } from 'redux-persist';
import Config from '@config';
import AppActions from '@redux-modules/App';

const updateReducers = (store) => {
	const reducerVersion = Config.ReduxPersist.reducerVersion;
	const config = Config.ReduxPersist.storeConfig;
	const init = () => store.dispatch(AppActions.init());

	// Check to ensure latest reducer version
	AsyncStorage.getItem('reducerVersion').then((localVersion) => {
		if (localVersion !== reducerVersion) {
			// Purge store
			persistStore(store, config, init).purge();
			AsyncStorage.setItem('reducerVersion', reducerVersion);
			AsyncStorage.removeItem('accessToken');
		} else {
			persistStore(store, config, init);
		}
	}).catch(() => {
		persistStore(store, config, init);
		AsyncStorage.setItem('reducerVersion', reducerVersion);
	});
};

export default { updateReducers };
