export default {
	DebugSettings: {
		yellowBox    : false,
		reduxLogging : false,
		useReduxDevTools : false,
	},

	ReduxPersist: {
		reducerVersion : '1.3',
	}
};
