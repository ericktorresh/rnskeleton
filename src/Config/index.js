import merge from 'lodash/merge';
import BaseSettings from './base';
import LocalSettings from './local';

const local = merge({}, BaseSettings, LocalSettings);

if (__DEV__) {
	// If ReactNative's yellow box warnings are too much, it is possible to turn
	// it off, but the healthier approach is to fix the warnings.  =)
	console.disableYellowBox = !local.DebugSettings.yellowBox;
}

export default local;
