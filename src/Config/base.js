// import immutablePersistenceTransform from '../Services/ImmutablePersistenceTransform'
import { AsyncStorage } from 'react-native';

const API = {
	baseUrl : 'http://api.localhost.com/',
	timeout : 10000
};

const DebugSettings = {
	useFixtures      : __DEV__,
	yellowBox        : __DEV__,
	reduxLogging     : __DEV__,
	useReactotron    : __DEV__,
	useReduxDevTools : __DEV__,
};

const ReduxPersist = {
	active         : true,
	reducerVersion : '1',
	storeConfig    : {
		storage        : AsyncStorage,
		blacklist      : ['app', 'nav'],
		//transforms     : [immutablePersistenceTransform]
	}
};

export default { API, DebugSettings, ReduxPersist };
