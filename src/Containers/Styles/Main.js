// @flow
import { StyleSheet, Platform } from 'react-native';
import { ApplicationStyles, Metrics, Colors } from '@theme';

export default styles = StyleSheet.create({
	...ApplicationStyles.screen,
	...ApplicationStyles.text,
	...ApplicationStyles.button,

	statusBar: {
		...Platform.select({
			android: {
				backgroundColor: Colors.mainColor,
				height: Metrics.statusBarHeight,
			},
		}),
	},
});
