// @flow
import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import HomeComponent from '@components/Home/HomeComponent';
import strings from '@I18n';
import styles from './Styles/Main';

class HomeContainer extends Component {
	static navigationOptions = {
		title: strings.home,
	};

	goToHello = () => {
		this.props.navigation.navigate('Hello');
	}

	render() {
		return (
			<View style={styles.container}>
				<HomeComponent
					token={this.props.token}
					actions={{
						goToHello: this.goToHello
					}}
				/>
			</View>
		);
	}
}

const mapStateToProps = (state) => ({
	token: state.app.token
});

const mapDispatchToProps = (/*dispatch*/) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);
