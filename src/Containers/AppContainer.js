// @flow
import React, { Component } from 'react';
import { Platform, StatusBar, View } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import AppActions from '@redux-modules/App';
import NavigationRouter from 'App/Navigation/NavigationRouter';
import Config from '@config';
import styles from './Styles/Main';

class AppContainer extends Component {
	componentDidMount() {
		if (!Config.ReduxPersist.active) {
			this.props.actions.app.init();
		}
	}

	render() {
		const statusBarConfig = Object.assign({}, {
			...Platform.select({
				ios: {
					animated: true,
					hidden: false,
					barStyle: 'default',
					networkActivityIndicatorVisible: false,
					showHideTransition: 'fade',
				},
				android: {
					animated: true,
					hidden: false,
					showHideTransition: 'fade',
					backgroundColor: 'rgba(0, 0, 0, 0.2)',
					translucent: true,
				}
			})
		}, {
			barStyle: 'default',
		});

		return (
			<View style={styles.application}>
				<View style={styles.statusBar}>
					<StatusBar {...statusBarConfig} />
				</View>
				<NavigationRouter />
			</View>
		);
	}
}

const mapStateToProps = (state) => ({
	app : state.app,
});

const mapDispatchToProps = (dispatch) => ({
	actions: {
		app : bindActionCreators(AppActions, dispatch),
	}
});

export default connect(mapStateToProps, mapDispatchToProps)(AppContainer);
