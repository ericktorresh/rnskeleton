// @flow
import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import HelloComponent from '@components/Home/HelloComponent';
import strings from '@I18n';
import styles from './Styles/Main';

class HelloContainer extends Component {
	static navigationOptions = {
		title: strings.hello,
	};

	goBack = () => {
		this.props.navigation.goBack();
	}

	render() {
		return (
			<View style={styles.container}>
				<HelloComponent
					actions={{
						goBack: this.goBack
					}}
				/>
			</View>
		);
	}
}

const mapStateToProps = (/*state*/) => ({});

const mapDispatchToProps = (/*dispatch*/) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(HelloContainer);
