// @flow
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import AppContainer from '@containers/AppContainer';
import createStore from 'App/Redux';

const store = createStore();

export default class Root extends Component {
	render() {
		return (
			<Provider store={store}>
				<AppContainer />
			</Provider>
		);
	}
}
