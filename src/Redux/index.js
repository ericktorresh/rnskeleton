// @flow
import { combineReducers } from 'redux';
import { reducer as AppReducer } from 'App/Redux/Modules/App';
import { reducer as NavReducer } from 'App/Redux/Modules/Nav';
import rootSaga from 'App/Redux/Sagas';
import configureStore from 'App/Redux/configureStore';

export default () => {
	/* ------------- Assemble The Reducers ------------- */
	const rootReducer = combineReducers({
		app: AppReducer,
		nav: NavReducer,
	});
	return configureStore(rootReducer, rootSaga, {});
};
