import { takeLatest, put, call } from 'redux-saga/effects';
import AppActions, { AppTypes } from '@redux-modules/App';

export function* init(api) {
	try {
		// make the call to the api
		const response = yield call(api.fetchToken);

		if (response.ok) {
			yield put(AppActions.initSuccess(response.data.token));
		} else {
			yield put(AppActions.initFailure(response.problem));
		}
	} catch (e) {
		yield put(AppActions.initFailure('Something went wrong.'));
	}
}

export function* watchInit(Api) {
	yield takeLatest(AppTypes.INIT, init, Api);
}
