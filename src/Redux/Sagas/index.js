// @flow
import { fork } from 'redux-saga/effects';
import Config from '@config';
import FixturesAPI from '../../Services/FixturesAPI';
import ApiService from '../../Services/Api';


/* ------------- Sagas ------------- */
import { watchInit } from './App';

/* ------------- API ------------- */
const Api = Config.DebugSettings.useFixtures === true ? FixturesAPI : ApiService.create(); // @TODO: Add real API service

export default function* root() {
	yield fork(watchInit, Api);
}
