// @flow
import { createLogger } from 'redux-logger';
import { autoRehydrate } from 'redux-persist';
import createSagaMiddleware from 'redux-saga';
import { applyMiddleware, compose, createStore } from 'redux';
import { composeWithDevTools } from 'remote-redux-devtools';
import RehydrationServices from 'App/Services/RehydrationServices';
import Config from '@config';

export default function configureStore(rootReducer, rootSaga, initialState) {
	const middleware = [];
	const enhancers = [];

	const sagaMonitor = __DEV__ ? console.tron.createSagaMonitor() : null;
	const sagaMiddleware = createSagaMiddleware({ sagaMonitor });
	middleware.push(sagaMiddleware);

		/* ------------- Logger Middleware ------------- */
	const LOGGING_BLACKLIST = ['EFFECT_TRIGGERED', 'EFFECT_RESOLVED', 'EFFECT_REJECTED', 'persist/REHYDRATE'];

	if (__DEV__) {
		const USE_LOGGING = Config.DebugSettings.reduxLogging;
		const logger = createLogger({
			predicate: (getState, { type }) => USE_LOGGING && !LOGGING_BLACKLIST.includes(type)
		});
		middleware.push(logger);
	}

	enhancers.push(applyMiddleware(...middleware));

	// add the autoRehydrate enhancer
	if (Config.ReduxPersist.active) {
		enhancers.push(autoRehydrate());
	}

	const appropiateCompose = Config.DebugSettings.useReduxDevTools ? composeWithDevTools({ realtime: true }) : compose;
	const createAppropriateStore = Config.DebugSettings.useReactotron ? console.tron.createStore : createStore;
	const store = createAppropriateStore(rootReducer, initialState, appropiateCompose(...enhancers));

	if (Config.ReduxPersist.active) {
		RehydrationServices.updateReducers(store);
	}

	sagaMiddleware.run(rootSaga);

	return store;
}
