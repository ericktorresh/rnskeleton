// @flow
import { createActions, createReducer } from 'reduxsauce';

/* ------------- Types and Action Creators ------------- */
const { Types, Creators } = createActions({
	init: null,
	initSuccess: ['token'],
	initFailure: ['error'],
	closeApp: null,
});

export const AppTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */
export const INITIAL_STATE = {
	isInitiated: false,
	error: null,
	token: null,
};

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {

	[Types.INIT_SUCCESS]: (state, action) => {
		return {
			...state,
			token: action.token,
			isInitiated: true,
			error: null,
		};
	},

	[Types.INIT_FAILURE]: (state, action) => {
		return {
			...state,
			isInitiated: false,
			error: action.error,
		};
	},
});
