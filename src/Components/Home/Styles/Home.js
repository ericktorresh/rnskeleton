// @flow
import { StyleSheet } from 'react-native';
import { ApplicationStyles } from '@theme';

export default styles = StyleSheet.create({
	...ApplicationStyles.screen,
	...ApplicationStyles.text,

	welcome: {
		...ApplicationStyles.text.heading1,
		textAlign: 'center',
	},

	instructions: {
		...ApplicationStyles.text.regularText,
	},
});
