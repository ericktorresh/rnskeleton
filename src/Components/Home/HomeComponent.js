// @flow
import React, { Component } from 'react';
import { Text, TouchableHighlight, View } from 'react-native';
import strings from '@I18n';
import styles from './Styles/Home';

class HomeComponent extends Component {

	render() {
		const { token, actions } = this.props;

		return (
			<View style={styles.centering}>
				<Text style={styles.welcome}>
					{strings.home}
				</Text>
				<Text style={styles.heading3}>
					{strings.formatString(strings.token, token)}
				</Text>
				<TouchableHighlight onPress={actions.goToHello}>
					<Text style={styles.instructions}>
						{strings.formatString(strings.goTo, 'Hello')}
					</Text>
				</TouchableHighlight>
			</View>
		);
	}
}

export default HomeComponent;
