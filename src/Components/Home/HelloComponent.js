// @flow
import React, { Component } from 'react';
import { Text, TouchableHighlight, View } from 'react-native';
import strings from '@I18n';
import styles from './Styles/Home';

class HelloComponent extends Component {

	render() {
		const { actions } = this.props;

		return (
			<View style={styles.centering}>
				<Text style={styles.welcome}>{strings.hello}</Text>
				<TouchableHighlight onPress={actions.goBack}>
					<Text style={styles.instructions}>{strings.goBack}</Text>
				</TouchableHighlight>
			</View>
		);
	}
}

export default HelloComponent;
