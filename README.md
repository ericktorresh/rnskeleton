#Init

### Important Note: this repo works with an existing installation of React Native. You must have an initiated project before doing the following instruction. (the reason is: rename an existing project is too tricky) ###

# Manually installation (Existing Installation)
- Change in `package.json`:
	- "react": "16.0.0-alpha.6" to "react": "16.0.0-alpha.3"
	Fix many issues

- Run:
`yarn add apisauce lodash moment react-native-vector-icons react-redux redux redux-logger redux-persist redux-saga reduxsauce react-native-localization prop-types`

- And:
`yarn add react-navigation@https://github.com/react-community/react-navigation.git`

- For develop:
`yarn add --dev jsx-control-statements eslint-plugin-jsx-control-statements reactotron-apisauce reactotron-react-native reactotron-redux reactotron-redux-saga remote-redux-devtools eslint-config-rallycoding`



- Add this line to the `.gitignore` file:
```# Local Config
src/Config/local.js```

- Add this line to `.babelrc`:

```
"plugins": ["jsx-control-statements"]```

- Rename `RNSkeletonApp` to the new app in `index.ios.js` and `index.android.js`
- Run `cp src/Config/local.default.js src/Config/local.js`
- Run `react-native link`

# Automatically (New installation)
- `yarn install`
- `./bin/init <ProjectName>`

-------
This skeleton is majorly based on the generated project structure of [Ignite](https://github.com/infinitered/ignite).
